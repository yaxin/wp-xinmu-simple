	<section id="sidebar">
		<ul>
			<?php if( function_exists('dynamic_sidebar') && dynamic_sidebar() ) : else : ?>
				<?php wp_list_pages('title_li=<h2>页面</h2>'); ?>
				<li><h2>分类目录</h2>
						<ul>
							<?php wp_list_cats('sort_column=name&optioncount=1&hierarchical=0'); ?>
						</ul>
				</li>
				<li><h2>文章归档</h2>
						<ul>
							<?php wp_get_archives('type=monthly'); ?>
						</ul>
				</li>
				<?php get_links_list(); ?>
				<li id="calendar"><h2>日历</h2>
					<?php get_calendar(); ?>
				</li>
				<li><h2>管理</h2>
					<ul>
						<?php wp_register(); ?>
						<li><?php wp_loginout(); ?></li>
						<?php wp_meta(); ?>
					</ul>
				</li>
			<?php endif; ?>
		</ul>
	</section>
	<span class="clear">clear</span>
</div>