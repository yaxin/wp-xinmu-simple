<!DOCTYPE HTML>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width" />
	<title><?php
		/*
		 * Print the <title> tag based on what is being viewed.
		 */
		global $page, $paged;
		wp_title( '|', true, 'right' );
		// Add the blog name.
		bloginfo( 'name' );
		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );
		?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
	<![endif]-->
	<?php
		if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
		wp_head(); 
	?>
	<link href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
</head>
<body <?php body_class(); ?>>
<div class="top">
	<span class="color1"></span>
	<span class="color2"></span>
	<span class="color3"></span>
	<span class="color4"></span>
</div>
<span class="clear"></span>
<header id="blog-header">
	<hgroup>
		<div id="site-title"><h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1></div>
		<!-- <div id="site-description"><h2></h2></div> -->
	</hgroup>
	<nav id="main-menu">
		<div id="innerMenu">
		<?php 
			$menu_args = array(
			'container' => '',//最外层容器的标签名，默认div
			'container_class' => '',//最外层容器的class名
			'container_id' => '',//最外层容器的id名
			'menu_class' => '',//导航菜单ul标签的class名
			'menu_id' => "nav_ul",//导航菜单ul标签的id名
			'echo' => true,//是否打印，默认是true，如果想将导航的代码作为赋值使用，可设置为false
			'fallback_cb' => 'the_main_nav',//备用的导航菜单函数，用于没有在后台设置导航时调用
			'before' => '',//显示在导航a标签之前
			'after' => '',//显示在导航a标签之后
			'link_before' => '',//显示在导航链接名之前
			'link_after' => '',//显示在导航链接名之后
			'depth' => 0,//显示的菜单层数，默认0，0是显示所有层
			'walker' => new Walker_Nav_Menu(),//调用一个对象定义显示导航菜单
			'theme_location' => 'primary',//指定显示的导航名，如果没有设置，则显示第一个
			);
			wp_nav_menu( $menu_args ); 
		?>
			<div class="header-search">
				<?php include(TEMPLATEPATH . '/searchform.php'); ?>
			</div>
			<span class="clear">clear</span>
		</div>
	</nav> <!-- #main-menu -->
</header> <!-- #blog-header -->
