<?php
//导航栏配置
if (function_exists('add_theme_support')) {
	//开启导航菜单主题支持
	add_theme_support('nav-menus');
	//注册一个导航菜单
	register_nav_menus( array( 'primary' => __( 'Primary Navigation', 'primary' ) ) );
}
//没有在后台设置导航菜单时调用的函数
function the_main_nav() {
?>
<ul id="nav_ul">
	<li class="menu-item page_item home_page <?php if(is_home()) echo 'current-menu-item menu-item-home current_page_item'; ?>"><a href="<?php bloginfo('url'); ?>/">首页</a></li>
	<?php wp_list_pages('title_li=&depth=-1'); ?>
</ul>
<?php } ?>

<?php
//注册侧边栏
if (function_exists('register_sidebar'))
	register_sidebar();
?>

<?php 
function par_pagenavi($range = 9){
	// $paged - number of the current page
	global $paged, $wp_query;
	// How much pages do we have?
	if ( !$max_page ) {
		$max_page = $wp_query->max_num_pages;
	}
	// We need the pagination only if there are more than 1 page
	if($max_page > 1){
		if(!$paged){
			$paged = 1;
	}
	//echo '<span><strong>更多文章:&nbsp;</strong></span>';
	// On the first page, don't put the First page link
	/*
	if($paged != 1){
		echo "<a href='" . get_pagenum_link(1) . "' class='extend' title='跳转到首页'> 返回首页 </a>";
	}
	*/
	// To the previous page
	previous_posts_link('上一页');
	// We need the sliding effect only if there are more pages than is the sliding range
	if($max_page > $range){
	// When closer to the beginning
		if($paged < $range){
			for($i = 1; $i <= ($range + 1); $i++){
				echo "<a href='" . get_pagenum_link($i) ."'";
				if($i==$paged) 
					echo " class='current'";
				echo ">$i</a>";
			}
		}
		// When closer to the end
		elseif($paged >= ($max_page - ceil(($range/2)))){
			for($i = $max_page - $range; $i <= $max_page; $i++){
				echo "<a href='" . get_pagenum_link($i) ."'";
				if($i==$paged) 
					echo " class='current'";
				echo ">$i</a>";
			}
		}
	// Somewhere in the middle
	elseif($paged >= $range && $paged < ($max_page - ceil(($range/2)))){
		for($i = ($paged - ceil($range/2)); $i <= ($paged + ceil(($range/2))); $i++){
			echo "<a href='" . get_pagenum_link($i) ."'";
			if($i==$paged)
				echo " class='current'";
			echo ">$i</a>";
		}
	}
	}
	// Less pages than the range, no sliding effect needed
	else{
		for($i = 1; $i <= $max_page; $i++){
			echo "<a href='" . get_pagenum_link($i) ."'";
			if($i==$paged)
				echo " class='current'";
			echo ">$i</a>";
		}
	}
	// Next page
	next_posts_link('下一页');
	// On the last page, don't put the Last page link
	/*
	if($paged != $max_page){
		echo "<a href='" . get_pagenum_link($max_page) . "' class='extend' title='跳转到最后一页'> 最后一页 </a>";
	}
	* 
	*/
	}
}
?>

<?php //评论框dom树 ?>
<?php
if ( ! function_exists( 'xinmu_comment' ) ) :
function xinmu_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p>Pingback 引用通告：<?php comment_author_link(); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>" class="comment-wrapper">
			<div class="comment-meta">
				<div class="comment-author vcard">
					<?php echo get_avatar( $comment, 36 ); ?>
					<?php comment_author_link(); ?><span class="says"> 说：</span>
					<?php if ( $comment->comment_approved == '0' ) : ?>
						<em class="comment-awaiting-moderation" title="<?php comment_time('Y-n-j H:i') ?>">初次评论需要等待审核。</em>
					<?php else : ?>
						<time pubdate datetime="<?php comment_date() ?>"><?php echo comment_time('Y-n-j H:i'); ?></time>
					<?php endif; ?>
				</div><!-- .comment-author .vcard -->
			</div>
	
			<div class="comment-content"><?php comment_text(); ?></div>
	
			<?php if ( $comment->comment_approved != '0' && $args != null&&$depth != null) : ?>
			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
				<?php if(function_exists('mailtocommenter_button')) mailtocommenter_button();?>
			</div><!-- .reply -->
			<?php endif; ?>
		</div><!-- #comment-## -->
	
	<?php
			break;
	endswitch;
}
endif;
?>
<!-- End Of Function File -->