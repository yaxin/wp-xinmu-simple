<?php get_header(); ?>
<div id="container">
	<section id="content">
		<div class="innerContent">
		<?php if ( have_posts() ) : ?>
			<h1 class="search-result-title">搜索结果：<span class="search-word"><?php the_search_query(); ?></span></h1>
			<hr class="search-result-hr">
			<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header search-result-header">
					<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				</header>
				<div class="entry-content">
					<?php	echo mb_strimwidth(strip_tags($post->post_content),0,300,'......'); ?>						
				</div>
				<footer class="entry-footer">
					<div class="go-on-read"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark">继续阅读</a></div>
				</footer>
				<span class="clear">clear</span>
				<hr>
			</article>
			<?php endwhile; ?>
			<div id="navigation"><div class="page_nav"><?php par_pagenavi(9); ?></div></div>
		<?php else : ?>
			<article id="no-search-result">
				<header class="no-search-header">
					<h2 class="no-search-result-title">抱歉，没有符合您搜索条件的结果。</h2>
				</header>
				<div class="no-search-content">
					施主，你拿错钵了
				</div>
				<footer class="no-search-footer">
					
				</footer>
			</article>
		<?php endif; ?>
	</div>
	</section>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>