<?php get_header(); ?>
<div id="container">
	<section id="content">
		<div class="innerContent">
		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?> fully-post" <?php post_class(); ?>>
				<header class="post-header post-title the-title">
					<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				</header><!-- post-header -->
				<aside class="post-info the-info">
					<strong>/</strong>
					<span class="post-tags">
						<?php the_tags('<strong>标签：</strong> ', ' , ' , ''); ?>
					</span>
				</aside>
				<hr>
				<div class="post-entry the-post">
					<?php the_content(); ?>
					<div class="eof">--EOF--</div>			
				</div>
				<footer class="post-cate">
				</footer>
				<hr>
			</article>
			<?php comments_template( '', true ); ?>
		<?php endwhile; ?>
	</div>
	</section>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>