<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	</header>
	<aside class="entry-info">
		<strong>/</strong>
		<?php the_author_posts_link(); ?>
		发表于
		<time class="entry-date" datetime="<?php the_time('c'); ?>" pubdate><strong><?php the_date('Y-m-d'); ?></strong></time>
		<?php if ( comments_open() && ! post_password_required() ) : ?>
			<span class="comments-link">
				<?php comments_popup_link( __( '暂无评论' ), __( '1条评论' ), __( '%条评论' ) ); ?>
			</span>
		<?php endif; ?>
		<?php if(function_exists('the_views')) { the_views(); } ?>
	</aside>
	<div class="entry-content">
		<?php //the_excerpt(); ?>
		<?php //the_content('阅读全文...'); ?>
		<?php	echo mb_strimwidth(strip_tags($post->post_content),0,300,'......'); ?>						
	</div>
	<footer class="entry-footer">
		<div class="go-on-read"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark">继续阅读</a></div>
	</footer>
	<span class="clear">clear</span>
	<hr>
</article> <!-- article -->