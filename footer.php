	<footer id="footer">
		<div class="copyright"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></div>
		<div class="powerd">Powerd By <a href="http://cn.wordpress.org" target="_blank">WordPress</a> | Theme By <a href="http://xinmu.me">馨木</a></div>
	</footer>
<?php wp_footer(); ?>
</body>
</html>
