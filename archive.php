<?php get_header(); ?>
<div id="container">
	<section id="content">
		<div class="innerContent">
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header archive-header">
					<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				</header>
				<div class="entry-content">
					<?php //the_excerpt(); ?>
					<?php //the_content('阅读全文...'); ?>
					<?php echo mb_strimwidth(strip_tags($post->post_content),0,200,'......'); ?>
				</div>
				<footer class="entry-footer">
					<div class="go-on-read"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark">继续阅读</a></div>
				</footer>
				<span class="clear">clear</span>
				<hr>
			</article> <!-- article -->
			<?php endwhile; ?>
			<div id="navigation"><div class="page_nav"><?php par_pagenavi(9); ?></div></div>
		<?php else : ?>
			<article id="no-post no-archive">
				<header class="no-post-header entry-header">
					<h2 class="entry-title">嗯嗯，你进错地方了</h2>
				</header>
				<div class="no-post-content entry-content no-archive-content">
					施主，这里荒芜人烟，你最好换个地方去化斋
				</div>
				<footer class="no-post-footer entry-footer no-archive-footer">
					
				</footer>
			</article>
		<?php endif; ?>
	</div>
	</section>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>