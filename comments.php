<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to twentyeleven_comment() which is
 * located in the functions.php file.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
<div id="comments">
	<?php if ( post_password_required() ) : ?>
		<p class="nopassword">这是一篇受密码保护的文章，您需要输入密码查看评论。</p>
	</div><!-- #comments -->
	<?php
			/* Stop the rest of comments.php from being processed,
			 * but don't kill the script entirely -- we still have
			 * to fully load the template.
			 */
			return;
		endif;
	?>

	<?php // You can start editing here -- including this comment! ?>
	<?php if ( have_comments() ) : ?>
		<h2 id="comments-title"><?php comments_number('没有评论','已有 1 条评论','已有 % 条评论'); ?></h2>

		<ol id="commentlist" class="commentlist">
			<?php wp_list_comments( array( 'callback' => 'xinmu_comment' ) ); ?>
		</ol>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-below">
			<?php paginate_comments_links(); ?>
		</nav>
		<?php endif; // check for comment navigation ?>
	<?php endif; ?>

<?php if ( comments_open() ) : ?>
	<div id="respond">
		<h3><?php comment_form_title( '发表评论', '回复 %s 的评论' ); ?></h3>
		<?php cancel_comment_reply_link() ?>
		<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
			<p class="must-log-in">要发表评论，您必须先<a href="<?php echo wp_login_url(get_permalink()); ?>">登录</a>。</p>
		<?php else : ?>
			<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
				<?php comment_id_fields(); ?>
				<?php if ( is_user_logged_in() ) : ?>
					<p class="logged-in-as">以 <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a> 的身份登录。<a href="<?php echo wp_logout_url(get_permalink()); ?>" title="登出此账户">登出？</a></p>
				<?php else : ?>
					<div id="comment-userinfo">
						<p class="comment-form-author">
							<input id="author" name="author" type="text" value="<?php echo $comment_author; ?>" required>
							<label for="author">昵称</label><span class="required">*</span>
						</p>
						<p class="comment-form-email">
							<input id="email" name="email" type="email" value="<?php echo $comment_author_email; ?>" required>
							<label for="email">邮箱</label><span class="required">*</span>
						</p>
						<p class="comment-form-url">
							<input id="url" name="url" type="text" value="<?php echo $comment_author_url; ?>">
							<label for="url">主页</label>
						</p>
					</div>
					<span class="clear">clear</span>
				<?php endif; ?>
				<p class="comment-form-comment">
					<!-- <textarea id="comment" name="comment" rows="10" required onfocus="this.previousSibling.style.display='none'" onblur="this.previousSibling.style.display=this.value==''?'block':'none'"></textarea> -->
					<textarea id="comment" name="comment" rows="10" required ></textarea>
				</p>
				<div class="comment-bottom">
					<div class="comment-more">
						<?php if ( function_exists(cs_print_smilies) ) : ?>
						<div class="comment-smilies">
							<?php cs_print_smilies(); ?>
						</div>
						<?php endif; ?>
					</div>
					<input name="submit" type="submit" id="submit" value="发表评论">
					<div class="patch"><?php do_action('comment_form', $post->ID); ?></div>
				</div>
				<span class="clear">clear</span>
				<script>//document.getElementById("comment").onkeydown=function ctrlEnter(e){if(!e)var e=window.event;if(e.ctrlKey&&e.keyCode==13){document.getElementById("submit").click();}}</script>
			</form>
		<?php endif; ?>
	</div>
<?php else : ?>
	<p class="nocomments">评论已关闭。</p>
<?php endif; ?>
</div><!-- #comments -->
