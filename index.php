<?php get_header(); ?>
<div id="container">
	<section id="content">
		<div class="innerContent">
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php include(TEMPLATEPATH . '/index-loop.php'); ?>
			<?php endwhile; ?>
			<div id="navigation"><div class="page_nav"><?php par_pagenavi(9); ?></div></div>
		<?php else : ?>
			<article id="no-post" class="post no-results not-found">
				<header class="no-post-header entry-header">
					<h2 class="entry-title">嗯嗯，你进错地方了</h2>
				</header>
				<div class="no-post-content entry-content">
					施主，这里荒芜人烟，你最好换个地方去化斋
				</div>
				<footer class="no-post-footer entry-footer">
					
				</footer>
			</article><!-- #post-0 -->
		<?php endif; ?>
		</div><!-- .innerContent -->
	</section><!-- #content -->
	<?php get_sidebar(); ?>
<?php get_footer(); ?>