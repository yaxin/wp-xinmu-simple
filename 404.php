<!DOCTYPE HTML>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width" />
	<title>呜...404</title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
	<![endif]-->
	<?php
		/* Always have wp_head() just before the closing </head>
		 * tag of your theme, or you will break many plugins, which
		 * generally use this hook to add elements to <head> such
		 * as styles, scripts, and meta tags.
		 */
		wp_head();
	?>
	<link href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
</head>
<body <?php body_class(); ?>>
<div class="top">
	<span class="color1"></span>
	<span class="color2"></span>
	<span class="color3"></span>
	<span class="color4"></span>
</div>
<span class="clear"></span>
<div id="page404">
	<p class="a">404，你懂的</p>
	<p>如果你不知道什么是404，点击查看<a href="http://en.wikipedia.org/wiki/HTTP_404" target="_blank">HTTP 404</a></p>
	<div class="back">
		<span id="goBack" onclick="javascript:history.go(-1)">返回上一页</span>
		<a id="goHome" href="<?php echo esc_url( home_url( '/' ) ); ?>">回主页</a>
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>