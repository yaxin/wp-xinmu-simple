<?php get_header(); ?>
<div id="container">
	<section id="content">
		<div class="innerContent">
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="page-header post-title the-title">
						<h2 class="page-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					</header>
					<aside class="post-info page-info the-info">
						<strong>/</strong>
						<?php the_author_posts_link(); ?>
						发表于
						<time class="entry-date" datetime="<?php the_time('c'); ?>" pubdate><strong><?php the_date('Y-m-d'); ?></strong></time>
						<?php if ( comments_open() && ! post_password_required() ) : ?>
							<span class="comments-link">
								<?php comments_popup_link( __( '暂无评论' ), __( '1条评论' ), __( '%条评论' ) ); ?>
							</span>
						<?php endif; ?>
						<?php if(function_exists('the_views')) { the_views(); } ?>
					</aside>
					<hr>
					<div class="page-entry post-entry the-post">
						<?php the_content(); ?>					
					</div>
					<footer class="post-meta">
					</footer>
					<span class="clear">clear</span>
					<hr>
				</article> <!-- article -->
				<?php comments_template( '', true ); ?>
			<?php endwhile; ?>
		</div>
	</section>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>